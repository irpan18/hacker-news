import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  isDialogOpen = false;

  constructor(private modalService: NgbModal) {}

  openDialog(content: any): void {
    if (!this.isDialogOpen) {
      this.isDialogOpen = true;
      const modalRef = this.modalService.open(content, { centered: true });
      modalRef.result.then(
        () => {
          this.isDialogOpen = false;
        },
        () => {
          this.isDialogOpen = false;
        }
      );
    }
  }
}
