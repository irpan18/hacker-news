import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardNewsCmpComponent } from './card-news-cmp.component';

describe('CardNewsCmpComponent', () => {
  let component: CardNewsCmpComponent;
  let fixture: ComponentFixture<CardNewsCmpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardNewsCmpComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CardNewsCmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
