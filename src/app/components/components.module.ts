import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { CardNewsCmpComponent } from './card-news-cmp/card-news-cmp.component';
import { ListNewsCmpComponent } from './list-news-cmp/list-news-cmp.component';
import { LoaderComponent } from './loader/loader.component';
import { DialogNewsCmpComponent } from './dialog-news-cmp/dialog-news-cmp.component';

@NgModule({
  declarations: [
    NavbarComponent,
    FooterComponent,
    CardNewsCmpComponent,
    ListNewsCmpComponent,
    LoaderComponent,
    DialogNewsCmpComponent,
  ],
  imports: [CommonModule],
  exports: [
    NavbarComponent,
    CardNewsCmpComponent,
    FooterComponent,
    ListNewsCmpComponent,
    LoaderComponent,
  ],
})
export class ComponentsModule {}
