import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/service/api.service';
import * as moment from 'moment';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-dialog-news-cmp',
  templateUrl: './dialog-news-cmp.component.html',
  styleUrls: ['./dialog-news-cmp.component.css'],
})
export class DialogNewsCmpComponent implements OnInit {
  @Input() dataItem: any;
  @Input() id: number = 0;
  loading: boolean = false;
  comments: any[] = [];
  commentsNested: any[] = [];
  timeAgo: string = '';

  constructor(
    public activeModal: NgbActiveModal,
    private service: ApiService
  ) {}

  ngOnInit(): void {
    this.getNewsItemComments(this.id);
    if (this.comments) {
    }
  }

  getNewsItemComments(newsId: number) {
    this.loading = true;
    this.service.getNewsItemComments(newsId).subscribe({
      next: (data) => {
        const commentIds = data.kids;
        if (commentIds) {
          for (let id of commentIds) {
            this.service.getNewsItem(id).subscribe({
              next: (detail: any) => {
                this.comments.push(detail);
                if (this.comments.length > 0) {
                  this.getNestedComments();
                }
              },
              error: (e) => {
                this.loading = false;
                console.log(e);
              },
            });
          }
        }
      },
      error: (e) => {
        this.loading = false;
        console.log(e);
      },
    });
  }

  close(): void {
    this.activeModal.dismiss();
  }

  returnTimeAgo(timestamp: number) {
    if (timestamp) {
      return (this.timeAgo = moment.unix(timestamp).fromNow());
    } else {
      return 'undefined';
    }
  }

  returnValue(value: any) {
    if (value) {
      return value;
    } else {
      return 'undefined';
    }
  }

  getNestedComments(): void {
    const commentIds = this.comments.map((comment) => comment.id);
    const nestedCommentsObservables =
      this.service.getNestedComments(commentIds);

    this.loading = true;
    forkJoin(nestedCommentsObservables).subscribe(
      (nestedComments) => {
        this.loading = false;
        this.commentsNested = nestedComments;
        console.log(this.commentsNested);
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
