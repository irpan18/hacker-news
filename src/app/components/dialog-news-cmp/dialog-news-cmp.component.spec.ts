import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogNewsCmpComponent } from './dialog-news-cmp.component';

describe('DialogNewsCmpComponent', () => {
  let component: DialogNewsCmpComponent;
  let fixture: ComponentFixture<DialogNewsCmpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogNewsCmpComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DialogNewsCmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
