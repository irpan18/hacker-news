import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListNewsCmpComponent } from './list-news-cmp.component';

describe('ListNewsCmpComponent', () => {
  let component: ListNewsCmpComponent;
  let fixture: ComponentFixture<ListNewsCmpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListNewsCmpComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListNewsCmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
