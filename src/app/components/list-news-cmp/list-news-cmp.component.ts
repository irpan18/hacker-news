import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { DialogNewsCmpComponent } from '../dialog-news-cmp/dialog-news-cmp.component';

@Component({
  selector: 'app-list-news-cmp',
  templateUrl: './list-news-cmp.component.html',
  styleUrls: ['./list-news-cmp.component.css'],
})
export class ListNewsCmpComponent implements OnInit {
  @Input() data: any | undefined;
  timeAgo: string = '';

  constructor(private modalService: NgbModal) {}

  ngOnInit(): void {}

  returnTimeAgo(timestamp: number) {
    if (timestamp) {
      return (this.timeAgo = moment.unix(timestamp).fromNow());
    } else {
      return 'undefined';
    }
  }

  returnValue(value: any) {
    if (value) {
      return value;
    } else {
      return 'undefined';
    }
  }

  openDialog(): void {
    const modalRef = this.modalService.open(DialogNewsCmpComponent, {
      size: 'lg',
      centered: true,
    });
    modalRef.componentInstance.dataItem = this.data;
    modalRef.componentInstance.id = this.data.id;
  }
}
