import { Component, OnInit } from '@angular/core';
import { ApiService } from './service/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'hacker-news';

  data: any[] = [];
  groupedData: any[][] = [];
  loading = false;

  constructor(private service: ApiService) {}

  ngOnInit(): void {
    this.getNews();
  }

  getNews() {
    this.loading = true;
    this.service.getNews().subscribe({
      next: (arrId: number[]) => {
        const per20data = arrId.slice(0, 20);
        if (per20data) {
          for (let id of per20data) {
            this.service.getNewsItem(id).subscribe({
              next: (detail: any) => {
                this.loading = false;
                this.data.push(detail);
                if (this.data.length === per20data.length) {
                  this.groupData();
                }
              },
              error: (e) => {
                this.loading = false;
                console.log(e);
              },
            });
          }
        }
      },
      error: (e) => {
        this.loading = false;
        console.log(e);
      },
    });
  }

  groupData(): void {
    for (let i = 0; i < this.data.length; i += 4) {
      this.groupedData.push(this.data.slice(i, i + 4));
    }
  }
}
